$(function(){
    $('#add-customer').validate({
        rules:{
            'first_name':{
                required:true,
                minlength : 3,
                maxlength : 50,
            },
            'last_name':{
                required:true,
                minlength : 3,
                maxlength : 50,
            },
            'gst_no':{
                required:true,
                minlength : 3,
                maxlength : 50,
            },
            'phone_no':{
                required:true,
                minlength : 3,
                maxlength : 50,
            },
            'email_id':{
                required:true,
                minlength : 3,
                maxlength : 50,
            },
            'gender':{
                required:true,
                minlength : 3,
                maxlength : 50,
            },
            'block_no':{
                required:true,
                minlength : 3,
                maxlength : 50,
            },
            'street':{
                required:true,
                minlength : 3,
                maxlength : 50,
            },
            'city':{
                required:true,
                minlength : 3,
                maxlength : 50,
            },
            'pincode':{
                required:true,
                minlength : 3,
                maxlength : 50,
            },
            'state':{
                required:true,
                minlength : 3,
                maxlength : 50,
            },
            'town':{
                required:true,
                minlength : 3,
                maxlength : 50,
            },
            'country':{
                required:true,
                minlength : 3,
                maxlength : 50,
            }
        },
        
        submitHandler: function(form){
            form.submit();
        }
    })
});